from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.decorators.vary import vary_on_cookie
from django.views.decorators.cache import cache_control
from django.views.generic import (View, TemplateView, DetailView, DeleteView,
                                  CreateView, FormView)
from django.views.generic.edit import SingleObjectMixin, FormMixin
from django.http import (HttpResponseRedirect, HttpResponseForbidden,
                         HttpResponse, JsonResponse, HttpResponseNotFound,
                         HttpResponseServerError)
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.translation import ugettext as _
from django.db.models import Max

from .models import Announcement, Thread, unreadThread, Participant, Message
from .forms import MessageForm, ThreadForm, AjaxAnnouncementForm
from sis_tools.tools import HttpResponseCode, get_object_or_None, getattrd
from sis_tools.views import (paginate, ajaxBootstrapEditableView, ListView,
                             NavBarMixin, DeleteRedirectView,
                             BootstrapEditableView)


class HomePageView(NavBarMixin, TemplateView):
    active = 0
    subactive = 0
    # TODO: replace me with standart
    template_name = "sis_main/dashboard.html"


@login_required
def handler404(request):
    return render(request, 'sis_main/404.html', {})


# ######################## Announcement system views ##########################
class ListAnnouncements(NavBarMixin, ListView):
    model = Announcement
    paginate_by = 15
    paginate_orphans = 5
    active = 0
    subactive = 1
    # TODO: replace me with standart
    template_name = 'sis_main/listAnnouncements.html'


class DetailAnnouncement(NavBarMixin, DetailView):
    model = Announcement
    active = 0
    subactive = 1
    # TODO: replace me with standart
    template_name = 'sis_main/showAnnouncement.html'
    context_object_name = "obj"


class AddAnnouncement(PermissionRequiredMixin, View):
    permission_required = 'sis_main.add_announcement'

    def post(self, request, *args, **kwargs):
        ann = Announcement(text=request.POST["text"], show=False)
        ann.save()
        return JsonResponse({"ID": ann.id})


class DeleteAnnouncement(PermissionRequiredMixin, DeleteRedirectView):
    model = Announcement
    success_url = reverse_lazy("listAnnouncements")
    permission_required = 'sis_main.delete_announcement'

    def get_success_url(self):
        return "%s?del=1" % super(DeleteAnnouncement, self).get_success_url()


class EditAnnouncement(PermissionRequiredMixin, BootstrapEditableView):
    permission_required = 'sis_main.change_announcement'
    form_class = AjaxAnnouncementForm
    model = Announcement
    fields = {"shower": ("show", "show"), "name": ("text", "text")}


# ########################## Messages system views ############################
class ShowThread(NavBarMixin, FormMixin, SingleObjectMixin, ListView):
    active = 0
    form_class = MessageForm
    paginate_by = 15
    participant = None
    # TODO: replace me with standart
    template_name = 'sis_main/showThread.html'

    def get_success_url(self):
        return "%s?page=last" % reverse('showThread',
                                        kwargs={'pk': self.object.pk})

    def get_participant(self):
        part = get_object_or_None(Participant, thread=self.object,
                                  user=self.request.user)
        if part is not None:  # remove unreadThread Record
            try:
                unreadThread.objects.get(participant=part,
                                         thread=self.object).delete()
            except unreadThread.DoesNotExist:
                pass
        return part

    def get_context_data(self, **kwargs):
        context = super(ShowThread, self).get_context_data(**kwargs)
        if (not self.object.isLocked and
                getattr(self.participant, 'isActive', False)):
            context['form'] = self.get_form()
        else:
            context['form'] = None
        context['thread'] = self.object
        context['part'] = self.participant
        return context

    def get_queryset(self):
        return self.object.messages.all()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Thread.objects.all())
        self.participant = self.get_participant()
        if (self.participant is None and not
                request.user.has_perm('sis_main.can_see_all')):
            return HttpResponseForbidden()
        return super(ShowThread, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Thread.objects.all())
        self.participant = self.get_participant()
        if (self.participant is None and not
                request.user.has_perm('sis_main.can_see_all')):
            return HttpResponseForbidden()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        Message(participant=self.participant,  thread=self.object,
                title=form.cleaned_data["title"],
                text=form.cleaned_data["text"]).save()
        return super(ShowThread, self).form_valid(form)


class ListUserThreads(NavBarMixin, ListView):
    active = 0
    subactive = 2
    paginate_by = 15
    template_name = "sis_main/listThreads.html"

    def get_queryset(self):
        return (self.request.user.participates.filter(isActive=True)
                    .annotate(max_date=Max("thread__messages__createdAt"))
                    .order_by("-max_date"))

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ListUserThreads, self).get_context_data(**kwargs)
        # Add in participant flag
        context['participant'] = True
        return context


class ListAdminThreads(PermissionRequiredMixin, NavBarMixin, ListView):
    active = 0
    subactive = 2
    paginate_by = 15
    queryset = Thread.objects.annotate(max_date=Max(
                    "messages__createdAt")).order_by("-max_date")
    permission_required = "sis_main.threads_can_see_all"
    template_name = "sis_main/listThreads.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ListAdminThreads, self).get_context_data(**kwargs)
        # Add in participant flag
        context['participant'] = False
        return context


class ListThreads(View):
    def get(self, request, *args, **kwargs):
        if (request.GET.get("all") and
                request.user.has_perm('sis_main.threads_can_see_all')):
            view = ListAdminThreads.as_view()
        else:
            view = ListUserThreads.as_view()
        return view(request, *args, **kwargs)


class AddThread(NavBarMixin, PermissionRequiredMixin, FormView):
    permission_required = "sis_main.add_thread"
    active = 0
    form_class = ThreadForm
    users = None
    # TODO: replace me with standart
    template_name = "sis_main/addThread.html"

    def form_valid(self, form):
        thread = Thread(title=form.cleaned_data["title"])
        thread.save()

        part = Participant(user=self.request.user, thread=thread, isAdmin=True)
        part.save()

        for info in form.cleaned_data["users"]:
            u_part = Participant(user=info.user, thread=thread)
            u_part.save()

        message = Message(participant=part, thread=thread,
                          title=form.cleaned_data["title"],
                          text=form.cleaned_data["message"])
        message.save()
        self.success_url = thread.get_absolute_url()
        return super(AddThread, self).form_valid(form)

    def form_invalid(self, form):
        if form["users"].errors == []:
            self.users = form.cleaned_data["users"]
        return super(AddThread, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        if self.users:
            # Call the base implementation first to get a context
            context = super(AddThread, self).get_context_data(**kwargs)
            # Add in users
            context["users"] = self.users
            return context
        else:
            return super(AddThread, self).get_context_data(**kwargs)


class ShowThreadMeta(NavBarMixin, SingleObjectMixin, ListView):
    paginate_by = 30
    active = 0
    participant = None
    # TODO: replace me with standart
    template_name = 'sis_main/showThreadMeta.html'

    def get_participant(self):
        return get_object_or_None(Participant, thread=self.object,
                                  user=self.request.user)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Thread.objects.all())
        self.participant = self.get_participant()
        if self.participant is None and not self.request.user.has_perm(
                "sis_main.threads_can_see_all"):
            return HttpResponseForbidden()
        return super(ShowThreadMeta, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ShowThreadMeta, self).get_context_data(**kwargs)
        # TODO: replace me with standart
        context['obj'] = self.object
        context['part'] = self.participant
        return context

    def get_queryset(self):
        return self.object.participants.all()


class EditThread(BootstrapEditableView):
    form_class = ThreadForm
    model = Thread
    fields = {"title": ("title", "title")}

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        part = get_object_or_None(Participant, thread=self.object,
                                  user=self.request.user)
        if part is None and not request.user.has_perm('sis_main.change_thread'):
            return HttpResponseForbidden()
        elif not part.isAdmin:
            return HttpResponseForbidden()
        return super(EditThread, self).post(request, *args, **kwargs)


class DeleteThread(PermissionRequiredMixin, DeleteRedirectView):
    model = Thread
    success_url = reverse_lazy("listThreads")
    permission_required = 'sis_main.delete_thread'

    def get_success_url(self):
        return "%s?tr=1" % super(DeleteThread, self).get_success_url()


class RelThreadParticipant(View):
    actions = ["add", "remove", "reinvite"]

    class add(SingleObjectMixin, View):
        model = Thread
        pk_url_kwarg = 'thread_pk'

        def action(self):
            self.object = self.get_object()
            pk = self.kwargs.get('pk', None)

            user_part = get_object_or_None(Participant, thread=self.object,
                                           user=self.request.user)
            if (self.request.user.has_perm('sis_main.change_thread') or
                    getattr(user_part, 'isAdmin', False)):
                u = get_object_or_None(User, pk=pk)
                if u is None:
                    return False, 404
                part = Participant(user=u, thread=self.object)
                part.save()
                return True, 200
            else:
                return False, 403

        def get(self, request, *args, **kwargs):
            done, code = self.action()
            if done:
                return HttpResponseRedirect(reverse(
                    "showThreadMeta", args=[self.kwargs.get('thread_pk')]))
            elif code == 404:
                return HttpResponseNotFound()
            elif code == 403:
                return HttpResponseForbidden()
            else:
                return HttpResponseServerError()

        def post(self, request, *args, **kwargs):
            one, code = self.action()
            return HttpResponseCode(code)

    class remove(SingleObjectMixin, View):
        model = Thread
        pk_url_kwarg = 'thread_pk'
        part = None

        def action(self):
            self.object = self.get_object()
            pk = self.kwargs.get('pk', None)
            self.part = get_object_or_None(Participant, pk=pk)
            user_part = get_object_or_None(Participant, thread=self.object,
                                           user=self.request.user)
            if self.part is None:
                return False, 404

            if (self.part.user.pk == self.request.user.pk or
                    self.request.user.has_perm('sis_main.change_thread') or
                    getattr(user_part, 'isAdmin', False) and (
                    getattr(user_part, 'isAdmin', True) is not True or
                    self.request.user.pk == self.object.originalPoster.pk)):
                self.part.isActive = False
                self.part.save()
                return True, 200
            else:
                return False, 403

        def get(self, request, *args, **kwargs):
            done, code = self.action()
            if done:
                if request.user.pk == getattrd(self.part, 'user.pk', None):
                    return HttpResponseRedirect(reverse("listThreads"))
                else:
                    return HttpResponseRedirect(reverse(
                        "showThreadMeta", args=[self.kwargs.get('thread_pk')]))
            elif code == 404:
                return HttpResponseNotFound()
            elif code == 403:
                return HttpResponseForbidden()
            else:
                return HttpResponseServerError()

        def post(self, request, *args, **kwargs):
            one, code = self.action()
            return HttpResponseCode(code)

    class reinvite(SingleObjectMixin, View):
        model = Thread
        pk_url_kwarg = 'thread_pk'

        def action(self):
            self.object = self.get_object()
            pk = self.kwargs.get('pk', None)
            part = get_object_or_None(Participant, pk=pk)
            user_part = get_object_or_None(Participant, thread=self.object,
                                           user=self.request.user)
            if part is None:
                return False, 404

            if (self.request.user.has_perm('sis_main.change_thread') or
                    getattr(user_part, 'isAdmin', False)):
                part.isActive = True
                part.save()
                return True, 200
            else:
                return False, 403

        def get(self, request, *args, **kwargs):
            done, code = self.action()
            if done:
                return HttpResponseRedirect(reverse(
                        "showThreadMeta", args=[self.kwargs.get('thread_pk')]))
            elif code == 404:
                return HttpResponseNotFound()
            elif code == 403:
                return HttpResponseForbidden()
            else:
                return HttpResponseServerError()

        def post(self, request, *args, **kwargs):
            one, code = self.action()
            return HttpResponseCode(code)

    def post(self, request, *args, **kwargs):
        action = self.kwargs.get('action', None)
        action_list = {'add': self.add, 'remove': self.remove,
                       'reinvite': self.reinvite}
        if action in self.actions:
            return action_list[action].as_view()(request, *args, **kwargs)
        else:
            return HttpResponseCode(400)

    def get(self, request, *args, **kwargs):
        action = self.kwargs.get('action', None)
        action_list = {'add': self.add, 'remove': self.remove,
                       'reinvite': self.reinvite}
        if action in self.actions:
            return action_list[action].as_view()(request, *args, **kwargs)
        else:
            return HttpResponseBadRequest()

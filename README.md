# App main
## Models
### Model: Announcement(ModelBase)
Represents a site-wide announcement shown on home page

* id(AutoField) - 
* text(TextFieldStripped) - The text of this announcement
* creation_time(DateTimeField) - 
* show(BooleanField) - Should the text be showen to users?


### Model: Thread(ModelBase)
Thread of conversation

* *<ManyToOneRel: main.participant>*
* *<ManyToOneRel: main.unreadthread>*
* *<ManyToOneRel: main.message>*
* id(AutoField) - 
* title(CharFieldStripped) - The title of Conversation Thread.
* isLocked(BooleanField) - Is the Thread locked, therefore no Messages can be added?
* createdAt(DateTimeField) - Date and Time when the Thread was created.


### Model: Participant(ModelBase)
Participant of Thread

* *<OneToOneRel: main.unreadthread>*
* *<ManyToOneRel: main.message>*
* id(AutoField) - 
* user(ForeignKey -> main.models.Participant) - User which is Participant of a Thread.
* thread(ForeignKey -> main.models.Participant) - Thread in which User participates.
* isAdmin(BooleanField) - Is participant Administrator?
* isActive(BooleanField) - False when user leaved the Thred.


### Model: unreadThread(ModelBase)
Record that user have unread thread

* id(AutoField) - 
* participant(OneToOneField -> main.models.unreadThread) - Participant in a Thread.
* thread(ForeignKey -> main.models.unreadThread) - Thread in which Participantparticipates.


### Model: Message(ModelBase)
Message in Thread

* *<ManyToOneRel: main.fileattachment>*
* id(AutoField) - 
* participant(ForeignKey -> main.models.Message) - Author (Participant in Thread) of Message.
* thread(ForeignKey -> main.models.Message) - The Thread in which is the message.
* createdAt(DateTimeField) - Date and Time when the Message was created.
* title(CharFieldStripped) - Short Text title of Message.
* text(TextFieldStripped) - MARKDOWN text of message.


### Model: FileAttachment(ModelBase)
File Attachment for Message

* id(AutoField) - 
* message(ForeignKey -> main.models.FileAttachment) - Message to which is the file attached.
* file(FileField) - File Attachment of Message



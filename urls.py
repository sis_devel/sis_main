﻿from django.conf.urls import include, url
from django.views.generic import TemplateView

import django_js_reverse.views

from . import views
AJAXurlpatterns = [
    url(r'edit/thread/participant/(?P<thread_pk>[0-9]+)-(?P<pk>[0-9]+)/'
        r'(?P<action>\w+)$', views.RelThreadParticipant.as_view(),
        name="relThreadParticipant"),
]

urlpatterns = [
    url(r'^$', views.HomePageView.as_view(), name='home'),
    url(r'about$', TemplateView.as_view(template_name="sis_main/about.html"),
        name='about'),
    url(r'^jsreverse/$', django_js_reverse.views.urls_js, name='js_reverse'),
    url(r'ajax/', include(AJAXurlpatterns, namespace="ajax_main")),

    url(r'announcements/list$', views.ListAnnouncements.as_view(),
        name='listAnnouncements'),
    url(r'announcements/add$', views.AddAnnouncement.as_view(),
        name="addAnnouncement"),
    url(r'announcements/show/(?P<pk>[0-9]+)$',
        views.DetailAnnouncement.as_view(), name="showAnnouncement"),
    url(r'announcements/delete/(?P<pk>[0-9]+)$',
        views.DeleteAnnouncement.as_view(), name="deleteAnnouncement"),
    url(r'announcements/edit/(?P<pk>[0-9]+)$',
        views.EditAnnouncement.as_view(), name="editAnnouncement"),

    url(r'messages/show/(?P<pk>[0-9]+)$', views.ShowThread.as_view(),
        name="showThread"),
    url(r'messages/list/', views.ListThreads.as_view(),
        name="listThreads"),
    url(r'messages/thread/add$', views.AddThread.as_view(),
        name="addThread"),
    url(r'messages/thread/show/(?P<pk>[0-9]+)$',
        views.ShowThreadMeta.as_view(), name="showThreadMeta"),
    url(r'messages/thread/edit/(?P<pk>[0-9]+)$', views.EditThread.as_view(),
        name="editThread"),
    url(r'messages/thread/delete/(?P<pk>[0-9]+)$',
        views.DeleteThread.as_view(), name="deleteThread"),
              ]

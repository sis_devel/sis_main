from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _
from sis_users.models import Info
from .models import Announcement


class MessageForm(forms.Form):
    """ Sending Message"""
    title = forms.CharField(label=_("Titile"), required=False, max_length=30)
    text = forms.CharField(label=_("Text"),
                           widget=forms.Textarea, required=True)


class ThreadForm(forms.Form):
    """ Sending Message"""
    title = forms.CharField(label=_("Titile"), required=True, max_length=30)
    users = forms.ModelMultipleChoiceField(
                        queryset=Info.objects.all(),
                        label=_("Users"), required=True,
                        help_text=_("Choose users in thread."))
    message = forms.CharField(label=_("Message"),
                              widget=forms.Textarea, required=True)


class AjaxAnnouncementForm(forms.Form):
    show = forms.TypedChoiceField(
                   coerce=lambda x: x == '1',
                   choices=(('0', 0), ('1', 1)),
                   widget=forms.RadioSelect
                )
    text = forms.CharField(strip=True)

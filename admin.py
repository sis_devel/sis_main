from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.


from .models import *


# Register your models here
admin.site.register(Announcement)
admin.site.register(Thread)
admin.site.register(Participant)
admin.site.register(unreadThread)
admin.site.register(Message)
admin.site.register(FileAttachment)

from django import template
from ..models import Announcement

register = template.Library()


@register.inclusion_tag('sis_main/_announcement.html', takes_context=True)
def announce(context):
    a = Announcement.objects.filter(show=True).order_by("-creation_time")
    return {"objs": a, "perms": context["perms"]}

from django import template
from ..models import unreadThread

register = template.Library()


@register.assignment_tag(takes_context=True)
def count_unread_messages(context):
    try:
        request = context['request']
        return unreadThread.objects.filter(
            participant__user=request.user, participant__isActive=True).count()
    except Exception as e:
        return 0

﻿from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from stripped_fields.models.fields import CharFieldStripped, TextFieldStripped
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.utils import IntegrityError
from django.db import transaction


@python_2_unicode_compatible
class Announcement(models.Model):
    """Represents a site-wide announcement shown on home page"""

    text = TextFieldStripped(verbose_name=_("Announcement text"),
                             help_text=_("The text of this announcement"),
                             blank=False, null=False)

    creation_time = models.DateTimeField(auto_now_add=True,
                                         verbose_name=_("Date of Creation"))

    show = models.BooleanField(verbose_name=_("Show"),
                               help_text=_("Should the text be "
                                           "showen to users?"))

    def __str__(self):
        return self.text

    class Meta:  # pylint: disable=old-style-class

        """ Meta information for :py:class:`Announcement` """
        # pylint: disable=too-few-public-methods, no-init
        verbose_name = _('Announcement')
        verbose_name_plural = _('Announcements')


# ################################ Messages ##################################
@python_2_unicode_compatible
class Thread(models.Model):
    """ Thread of conversation"""

    title = CharFieldStripped(verbose_name=_("Title of Thread"),
                              help_text=_("The title of Conversation "
                              "Thread."), blank=False, null=False,
                              max_length=30)

    isLocked = models.BooleanField(verbose_name=_("Thread locked"),
                                   help_text=_("Is the Thread locked, "
                                   "therefore no Messages can be added?"),
                                   default=False)

    createdAt = models.DateTimeField(auto_now_add=True,
                                     verbose_name=_("Date and Time of "
                                                    "Creation"),
                                     help_text=_("Date and Time when the "
                                                 "Thread was created."))

    def __str__(self):
        return self.title

    @property
    def originalPoster(self):
        tmp = self.messages.order_by("createdAt").first()
        if tmp is not None:
            return tmp.participant.user
        else:
            return tmp

    @property
    def lastMessage(self):
        return self.messages.latest('createdAt')

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('showThreadMeta', args=[str(self.pk)])

    class Meta:  # pylint: disable=old-style-class

        """ Meta information for :py:class:`Thread` """
        # pylint: disable=too-few-public-methods, no-init
        verbose_name = _("Thread")
        verbose_name_plural = _("Threads")
        permissions = (('threads_can_see_all',
                        _("Can see all threads.")),)


@python_2_unicode_compatible
class Participant(models.Model):
    """ Participant of Thread """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"),
                             help_text=_("User which is Participant of a "
                                         "Thread."),
                             related_name="participates")

    thread = models.ForeignKey(Thread, verbose_name=_("Thread"),
                               help_text=_("Thread in which User "
                                           "participates."),
                               related_name="participants")

    isAdmin = models.BooleanField(default=False,
                                  verbose_name=_("Administrator"),
                                  help_text=_("Is participant "
                                              "Administrator?"))

    isActive = models.BooleanField(default=True,
                                   verbose_name=_("Is active"),
                                   help_text=_("False when user leaved "
                                               "the Thred."))

    def __str__(self):
        return self.user.get_full_name() + ": " + self.thread.title

    class Meta:  # pylint: disable=old-style-class

        """ Meta information for :py:class:`Participant` """
        # pylint: disable=too-few-public-methods, no-init
        verbose_name = _("Participant")
        verbose_name_plural = _("Participants")
        unique_together = ('user', 'thread')


@python_2_unicode_compatible
class unreadThread(models.Model):
    """ Record that user have unread thread"""

    participant = models.OneToOneField(Participant,
                                       verbose_name=_("Participant"),
                                       help_text=_("Participant in a Thread."),
                                       related_name="unread")

    thread = models.ForeignKey(Thread, verbose_name=_("Thread"),
                               help_text=_("Thread in which Participant"
                                           "participates."),
                               related_name="unreadparticipants")

    def __str__(self):
        return self.participant.user.get_full_name() + ": " + self.thread.title

    class Meta:  # pylint: disable=old-style-class

        """ Meta information for :py:class:`unreadThread` """
        # pylint: disable=too-few-public-methods, no-init
        verbose_name = _("Unread Thread")
        verbose_name_plural = _("Unread Threads")
        unique_together = ("participant", "thread")


@python_2_unicode_compatible
class Message(models.Model):
    """ Message in Thread"""

    participant = models.ForeignKey(Participant, verbose_name=_("Participant"),
                                    related_name="messages",
                                    help_text=_("Author (Participant in "
                                                "Thread) of Message."))

    thread = models.ForeignKey(Thread, verbose_name=_("Thread"),
                               help_text=_("The Thread in which is the "
                                           "message."),
                               related_name="messages")

    createdAt = models.DateTimeField(auto_now_add=True,
                                     verbose_name=_("Date of Creation"),
                                     help_text=_("Date and Time when the "
                                                 "Message was created."))

    title = CharFieldStripped(verbose_name=_("Title of Message"),
                              help_text=_("Short Text title of Message."),
                              blank=True, null=True, max_length=30)

    text = TextFieldStripped(verbose_name=_("Text of message"),
                             help_text=_("MARKDOWN text of message."))

    def __str__(self):
        if self.title is not None and self.title != "":
            return "%s: %s" % (self.thread.title, self.title)
        elif len(self.text) < 21:
            return "%s \"%s\"" % (self.thread.title,  self.text)
        else:
            return "%s \"%s...\"" % (self.thread.title,  self.text[:20])

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return "%s?page=last" % reverse('showThread',
                                        args=[str(self.thread.pk)])

    class Meta:  # pylint: disable=old-style-class

        """ Meta information for :py:class:`Message` """
        # pylint: disable=too-few-public-methods, no-init
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")


@python_2_unicode_compatible
class FileAttachment(models.Model):
    """ File Attachment for Message """

    message = models.ForeignKey(Message, verbose_name=_("Message"),
                                help_text=_("Message to which is the file "
                                            "attached."))

    file = models.FileField(verbose_name=_("File Attachment"),
                            help_text=_("File Attachment of Message"))

    def __str__(self):
        return "%s - %s" % (self.message, self.file)

    class Meta:  # pylint: disable=old-style-class

        """ Meta information for :py:class:`FileAttachment` """
        # pylint: disable=too-few-public-methods, no-init
        verbose_name = _("File Attachment")
        verbose_name_plural = _("File Attachments")

# ################################ signals ###################################

"""
post_save
=========

sender
    The model class.
instance
    The actual instance being saved.
created
    A boolean; True if a new record was created.
raw
    A boolean; True if the model is saved exactly as presented (i.e. when
    loading a fixture). One should not query/modify other records in the
    database as the database might not be in a consistent state yet.
using
    The database alias being used.
update_fields
    The set of fields to update explicitly specified in the save() method.
    None if this argument was not used in the save() call.

"""

"""
# TODO: this does no longer work implement other way?
@receiver(post_save, sender=Thread, weak=False)
def thread_post_save(sender, instance, created, raw, **kwargs):
    # When Thread is created creates new Participant with original_poster user
    if not raw and created:
        p = Participant(user=instance.originalPoster, thread=instance,
                        isAdmin=True)
        p.save()
"""


@receiver(post_save, sender=Message, weak=False)
def message_post_save(sender, instance, created, raw, **kwargs):
    """ When message is created makes the thread unread by participants"""
    if not raw and created:
        for part in instance.thread.participants.all():
            if part.pk != instance.participant.pk:
                try:
                    with transaction.atomic():
                        unreadThread.objects.create(participant=part,
                                                    thread=instance.thread)
                except IntegrityError:
                    pass
